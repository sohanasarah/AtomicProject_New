-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 29, 2016 at 10:38 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `atomicprojectb22`
--

-- --------------------------------------------------------

--
-- Table structure for table `birthday`
--

CREATE TABLE `birthday` (
  `id` int(11) NOT NULL,
  `b_day` date NOT NULL,
  `deleted_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `birthday`
--

INSERT INTO `birthday` (`id`, `b_day`, `deleted_at`) VALUES
(1, '1990-05-27', 1467201769),
(2, '1990-08-28', NULL),
(4, '0000-00-00', 1467228529),
(5, '0000-00-00', 1467095314),
(8, '0000-00-00', 1467095317),
(9, '1999-07-01', NULL),
(10, '1992-08-27', NULL),
(11, '1995-06-20', NULL),
(12, '1985-12-04', NULL),
(13, '1965-12-09', NULL),
(14, '1970-02-02', NULL),
(15, '1990-10-10', NULL),
(16, '1900-02-01', 1467201642),
(17, '1945-02-01', NULL),
(18, '2000-12-13', NULL),
(19, '2016-06-08', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `book`
--

CREATE TABLE `book` (
  `id` int(11) NOT NULL,
  `title` varchar(30) NOT NULL,
  `deleted_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book`
--

INSERT INTO `book` (`id`, `title`, `deleted_at`) VALUES
(1, 'book0', NULL),
(2, 'hello1', NULL),
(3, 'Clean code', NULL),
(4, 'The Alchemist', NULL),
(5, 'The Da Vinci Code', NULL),
(8, 'abcd', 1467096249),
(12, 'A game of thrones', NULL),
(16, 'abcdef', NULL),
(20, 'Harry Potter', NULL),
(21, 'Tom Sawyer', NULL),
(22, 'the lord of the rings', NULL),
(23, 'Hunger Games', NULL),
(24, '1234', 1467200899),
(25, 'clean code 2', 1467200922),
(26, 'abcdef', 1467200927);

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `city` varchar(200) NOT NULL,
  `deleted_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `city`, `deleted_at`) VALUES
(1, 'Dhaka', NULL),
(2, 'Chittagong', NULL),
(3, 'Sylhet', 1467047793),
(4, 'Dhaka', NULL),
(6, 'Sylhet', NULL),
(7, 'Jessore', NULL),
(8, 'Pabna', NULL),
(9, 'Rangpur', NULL),
(10, 'Rajshahi', NULL),
(11, 'Comilla', NULL),
(12, 'Comilla', 1467229035),
(13, 'Chittagong', NULL),
(14, 'Jessore', NULL),
(15, 'Sylhet', NULL),
(16, 'Rangpur', NULL),
(18, 'Pabna', 1467229051);

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE `email` (
  `id` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `deleted_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`id`, `email`, `deleted_at`) VALUES
(2, 'abcd@gmail.com', NULL),
(4, 'sohana@hotmail.com', NULL),
(11, 'email@gmail.com', NULL),
(12, '12345678@hotmail.com', NULL),
(14, 'example@yahoo.com', NULL),
(15, 'abcd@gmail.com', NULL),
(16, 'sohana_a27@yahoo.com', NULL),
(17, 'sherin@gmail.com', NULL),
(19, 'try@hotmail.com', 1467212758),
(20, '275@yahoo.com', 1467212761),
(21, 'newuser@outlook.com', NULL),
(22, 'example@aol.com', NULL),
(23, 'ayman@gmail.com', NULL),
(24, 'newuser@yahoo.com', NULL),
(25, '6thdf@hotmail.com', 1467213407),
(26, 'email@example.com', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE `gender` (
  `id` int(11) NOT NULL,
  `user` varchar(200) NOT NULL,
  `gender` varchar(200) NOT NULL,
  `deleted_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `user`, `gender`, `deleted_at`) VALUES
(1, 'Sarah', 'Female', NULL),
(2, 'Sohana', 'Female', 1467229523),
(4, 'Abid', 'Male', NULL),
(5, 'Sifat', 'Male', NULL),
(10, 'Jyoti', 'Female', NULL),
(11, 'Ayman', 'Female', NULL),
(14, 'Milon', 'Male', 1467229531),
(15, 'Karim', 'Male', NULL),
(16, 'Shahana', 'Male', 1467054348),
(17, 'Farah', 'Female', NULL),
(18, 'Fayaz', 'Male', NULL),
(19, '1234', 'Male', 1467229248),
(20, 'Raisa', 'Female', NULL),
(21, 'Sakib', 'Male', NULL),
(22, 'Xyz', 'Male', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hobby`
--

CREATE TABLE `hobby` (
  `id` int(11) NOT NULL,
  `hobbies` varchar(200) NOT NULL,
  `deleted_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hobby`
--

INSERT INTO `hobby` (`id`, `hobbies`, `deleted_at`) VALUES
(2, 'Gardening,Reading,Football,Cricket', NULL),
(3, 'Watching TV,Travelling,Swimming,Sleeping', NULL),
(8, 'Coding,Reading,Writing', NULL),
(9, 'Reading,Football,Cricket', NULL),
(10, 'Gardening,Coding', 1467203128),
(11, 'Football,Travelling,Swimming', NULL),
(12, 'Gardening,Reading', NULL),
(13, 'Coding', NULL),
(14, 'Reading', 1467230480),
(15, 'Gardening', 1467230484),
(18, 'Football,Cricket', NULL),
(19, 'Gardening,Coding', 1467230473),
(20, 'Watching TV,Sleeping,Cooking', 1467230442),
(21, 'Coding,Reading,Writing', NULL),
(22, 'Watching TV,Swimming', NULL),
(23, 'Football,Watching TV,Travelling,Swimming,Sleeping,Cooking', NULL),
(24, 'Reading,Travelling', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `profilepicture`
--

CREATE TABLE `profilepicture` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `images` varchar(255) NOT NULL,
  `deleted_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profilepicture`
--

INSERT INTO `profilepicture` (`id`, `name`, `images`, `deleted_at`) VALUES
(1, 'Sponge Bob', '1467215848sponge.jpg', NULL),
(2, 'Jerry', '1467216696jerry.jpg', NULL),
(6, 'Bird', '1467218351bird.png', NULL),
(7, 'Dora', '1467218264dora.jpg', NULL),
(8, 'minion', '1467218609minion.jpg', NULL),
(26, 'Pooh', '1467218861pooh.png', NULL),
(27, 'Angry Bird', '1467218883angrybird.png', NULL),
(28, 'mickey', '1467218963mickey.png', 1467219207),
(29, 'Mickey Mouse', '1467218992mickey2.png', NULL),
(31, 'Angry bird', '1467219077angrybirds2.jpg', NULL),
(32, 'angry 2', '1467219108angry.png', 1467219197),
(33, 'Minions', '1467219169images (1).jpg', 1467219499),
(34, 'Scooby', '1467219366scoby.png', NULL),
(35, 'Bugs Bunny', '1467219457bugs bunny.jpg', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `summary`
--

CREATE TABLE `summary` (
  `id` int(11) NOT NULL,
  `organization` varchar(100) NOT NULL,
  `summary` mediumtext NOT NULL,
  `deleted_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `summary`
--

INSERT INTO `summary` (`id`, `organization`, `summary`, `deleted_at`) VALUES
(1, 'abcd', 'abcd is a new company.', NULL),
(3, 'BITM', 'Bangladesh Association of Software and Information Services (BASIS) is the national trade body for Software & IT Enabled Service industry of Bangladesh. Established in 1997, the association has been working with a vision of developing vibrant software & IT service industry in the country. Currently BASIS has over 800 members who contribute the majority in the Software & IT service industry in Bangladesh.', NULL),
(4, 'Higher Achievement', 'Higher Achievement is a four-year high school preparatory after-school program for disadvantaged urban children of various academic levels during middle school, 5th-8th grade.', NULL),
(5, 'ABCD', 'asvsdgf', 1467211480),
(6, 'ABCD', 'This is a ', 1467211482),
(7, 'xyz', 'xyz is a', 1467211485),
(8, 'XYZ', 'XYZ Consulting is a new company that provides expertise in search marketing solutions for business on a worldwide basis, including website promotion, online advertising and search engine optimization techniques ', NULL),
(9, 'CUET', 'Chittagong University of Engineering and Technology commonly referred to as CUET, located in Chittagong, Bangladesh, is renowned as one of the public engineering universities in Bangladesh. The university maintains a special emphasis on research. ', NULL),
(10, 'AG', 'Software AG offers the first end-to-end Digital Business Platformâ€”based on open standards, with integration, process management, adaptive application development, real-time analytics and enterprise architecture management as core building blocksâ€”to help customers build their digital futures today. ', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birthday`
--
ALTER TABLE `birthday`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book`
--
ALTER TABLE `book`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobby`
--
ALTER TABLE `hobby`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profilepicture`
--
ALTER TABLE `profilepicture`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `summary`
--
ALTER TABLE `summary`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birthday`
--
ALTER TABLE `birthday`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `book`
--
ALTER TABLE `book`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `hobby`
--
ALTER TABLE `hobby`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `profilepicture`
--
ALTER TABLE `profilepicture`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `summary`
--
ALTER TABLE `summary`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
