-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 14, 2016 at 05:18 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `atomicprojectsarah`
--

-- --------------------------------------------------------

--
-- Table structure for table `book`
--

CREATE TABLE IF NOT EXISTS `book` (
`id` int(11) NOT NULL,
  `title` varchar(30) NOT NULL,
  `deleted_at` int(11) DEFAULT NULL,
  `description` varchar(200) NOT NULL,
  `strippedDesc` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book`
--

INSERT INTO `book` (`id`, `title`, `deleted_at`, `description`, `strippedDesc`) VALUES
(1, 'book', NULL, '', ''),
(2, 'hello1', NULL, '', ''),
(3, 'Clean code', NULL, '', ''),
(4, 'The Alchemist', NULL, '', ''),
(5, 'The Da Vinci Code', NULL, '', ''),
(8, 'abcd', 1467096249, '', ''),
(12, 'A game of thrones', NULL, '', ''),
(16, 'abcdef', NULL, '', ''),
(20, 'Harry Potter', NULL, '', ''),
(21, 'Tom Sawyer', NULL, '', ''),
(22, 'the lord of the rings', NULL, '', ''),
(23, 'Hunger Games', NULL, '', ''),
(24, '1234', 1467200899, '', ''),
(25, 'clean code 2', 1467200922, '', ''),
(26, 'abcdef', 1467200927, ' abcd   yr', 'abcd'),
(27, 'hello world', NULL, 'I am Sarah!', ''),
(28, '1234', NULL, 'hello there', ''),
(29, '', NULL, '', ''),
(30, '', NULL, ' abcd ', 'abcd'),
(31, 'new book2', NULL, '<p style="text-align: center;">Hello there <strong><em>Ayman</em></strong></p>', 'Hello there Ayman');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `book`
--
ALTER TABLE `book`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `book`
--
ALTER TABLE `book`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=32;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
