<?php
namespace App\Bitm\SEIP139942\Birthday;
use App\Bitm\SEIP139942\Message\Message;
use App\Bitm\SEIP139942\Utility\Utility;
class Birthday{
    public $id;
    public $bday;
    public $conn;

    public function __construct()
    {
        $this->conn=mysqli_connect("localhost","root","","atomicprojectsarah") or die ("Database connection failed");
    }

    public function prepare($data="")
    {
        if(array_key_exists("bday",$data))
        {
            $this->bday=$data['bday'];
        }
        if(array_key_exists("id",$data))
        {
            $this->id=$data['id'];
        }
    }

    public function store()
    {
        $query="INSERT INTO `atomicprojectbsarah`.`birthday` (`b_day`) VALUES ('".$this->bday."')";

        $result=mysqli_query($this->conn,$query);
        if($result)
        {
            Message::message("
        <div class=\"alert alert-success\">
        <strong>Success!</strong> Data has been stored successfully.
        </div>");
            Utility::redirect("index.php");
        }
        else{
            echo "Error!!!";
        }
    }


    public function index()
    {
        $_days=array();
        $query="SELECT * FROM `birthday` WHERE `deleted_at` IS NULL";
        $result=mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result))
        {
            $_days[]=$row;
        }
        return $_days;
    }

    public function view()
    {
        $query="SELECT * FROM `birthday` WHERE id=".$this->id;
        $result=mysqli_query($this->conn,$query);
        $row=mysqli_fetch_assoc($result);
        return $row;
    }

    public function update()
    {
        $query = "UPDATE `atomicprojectsarah`.`birthday` SET `b_day` = '".$this->bday."' WHERE `birthday`.`id` =".$this->id;
        $result = mysqli_query($this->conn,$query);
        if($result)
        {
            Message::message("
            <div class=\"alert alert-info\">
            <strong>Updated!</strong> Data has been updated.
            </div>");
            Utility::redirect('index.php');
        }
        else
        {
            echo "Error!";
        }
    }

    public function delete()
    {
        $query="DELETE FROM `atomicprojectsarah`.`birthday` WHERE `birthday`.`id`=".$this->id;
        $result = mysqli_query($this->conn,$query);
        if($result)
        {
            Message::message("
        <div class=\"alert alert-success\">
        <strong>Success!</strong> Data has been deleted successfully.
        </div>");
            Utility::redirect("index.php");
        }
        else{
            echo "Error!";
        }
    }
    public function trash(){
        $this->deleted_at=time();
        $query="UPDATE `atomicprojectsarah`.`birthday` SET `deleted_at` = '".$this->deleted_at."' WHERE `birthday`.`id` = " .$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
  <strong>Trashed!</strong> Data has been trashed successfully.
</div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been trashed successfully.
    </div>");
            Utility::redirect('index.php');

        }
    }
    public function trashed(){
        $_trashed=array();
        $query="SELECT * FROM `birthday` WHERE `deleted_at` IS NOT NULL";
        $result= mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result)){
            $_trashed[]=$row;
        }

        return $_trashed;


    }
    public function recover(){
        $query="UPDATE `atomicprojectsarah`.`birthday` SET `deleted_at` = NULL WHERE `birthday`.`id` = " .$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
  <strong>Trashed!</strong> Data has been trashed successfully.
</div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been trashed successfully.
    </div>");
            Utility::redirect('index.php');

        }

    }
    public function recoverMultiple($idS=array()){
        if((is_array($idS)) && count($idS)>0){
            $IDs= implode(",",$idS);
            $query="UPDATE `atomicprojectsarah`.`birthday` SET `deleted_at` = NULL  WHERE `birthday`.`id` IN(".$IDs.")";
            $result= mysqli_query($this->conn,$query);
            if($result){
                Message::message("<div class=\"alert alert-success\">
  <strong>Recovered!</strong> Selected Data has been recovered successfully.
</div>");
                header('Location:index.php');

            } else {
                Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Selected Data has not been recovered successfully.
    </div>");
                Utility::redirect('index.php');

            }
        }

    }
    public function deleteMultiple($idS=array()){
        if((is_array($idS)) && count($idS)>0){
            $IDs= implode(",",$idS);
            $query="DELETE FROM `atomicprojectsarah`.`birthday`  WHERE `birthday`.`id` IN(".$IDs.")";
            $result= mysqli_query($this->conn,$query);
            if($result){
                Message::message("<div class=\"alert alert-success\">
  <strong>Recovered!</strong> Selected Data has been deleted successfully.
</div>");
               header('Location:index.php');

            } else {
                Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Selected Data has not been deleted successfully.
    </div>");
               Utility::redirect('index.php');

            }
        }

    }
    public function count(){
        $query="SELECT COUNT(*) AS totalItem FROM `atomicprojectsarah`.`birthday` WHERE `deleted_at` IS NULL ";
        $result=mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row['totalItem'];
    }

    public function paginator($pageStartFrom=0,$Limit=5){
        $_allBday = array();
        $query="SELECT * FROM `birthday` WHERE deleted_at IS NULL LIMIT ".$pageStartFrom.",".$Limit;
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_assoc($result)) {
            $_allBday[] = $row;
        }
        return $_allBday;

    }



}