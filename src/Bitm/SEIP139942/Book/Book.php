<?php
namespace App\Bitm\SEIP139942\Book;
use App\Bitm\SEIP139942\Message\Message;
use App\Bitm\SEIP139942\Utility\Utility;

class Book{
    public $title="";
    public $id="";
    public $conn;
    public $deleted_at;

    public $filterByTitle="";
    public $filterByDescription="";
    public $search="";
    public $description;
    public $strippedDesc;



    public function prepare($data = "")
    {
        if (array_key_exists("title", $data)) {
            $this->title = $data['title'];
        }
        if (array_key_exists("description", $data)) {
            $this->description = $data['description'];
        }
        if (array_key_exists("description", $data)) {
            $this->strippedDesc = $data['strippedDesc'];
        }

        if (array_key_exists("filterByTitle", $data)) {
            $this->filterByTitle = $data['filterByTitle'];
        }
        if (array_key_exists("filterByDescription", $data)) {
            $this->filterByDescription = $data['filterByDescription'];
        }
        if (array_key_exists("search", $data)) {
            $this->search = $data['search'];
        }
        if (array_key_exists("id", $data)) {
            $this->id = $data['id'];
        }

        return $this;

    }
    public function __construct()
    {

        $this->conn=mysqli_connect("localhost","root","","atomicprojectsarah") or die("database connection failed");
    }


    public function store(){
        //$query = "INSERT INTO `atomicprojectsarah`.`book` (`title`, `description` , `strippedDesc`) VALUES ('".$this->title."', '".$this->description."','".$this->strippedDesc.")";
        $query="INSERT INTO `atomicprojectsarah`.`book` ( `title`, `description`, `strippedDesc`) VALUES ('".$this->title."', '".$this->description."', '".$this->strippedDesc."')";
        $result=mysqli_query($this->conn,$query);
        if($result){
            //echo "Data Has been stored successfully";
            Message::message("Data has been stored successfully");
            Utility::redirect("index.php");

        }
        else {
            echo "Error";
        }
    }
    public function index(){
        $whereClause= " 1=1 ";
        if(!empty($this->filterByTitle)){
            $whereClause.=" AND  title LIKE '%".$this->filterByTitle."%'";
        }
        if(!empty($this->filterByDescription)){
            $whereClause.=" AND  description LIKE '%".$this->filterByDescription."%'";
        }
        if(!empty($this->search)){
            $whereClause.=" AND  description LIKE '%".$this->search."%' OR title LIKE '%".$this->search."%'";
        }
        $_allBook=array();
        $query="SELECT * FROM `book` WHERE `deleted_at` IS NULL AND ".$whereClause;
        //Utility::dd($query);
        $result= mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result)){
            $_allBook[]=$row;
        }

        return $_allBook;


    }
    public function view(){
        $query="SELECT * FROM `book` WHERE `id`=".$this->id;
        $result= mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row;
    }

    public function update(){
        //$query="UPDATE `book` SET `title` = '".$this->title."' WHERE `book`.`id` = ".$this->id;
        $query="UPDATE `atomicprojectsarah`.`book` SET `title` = '".$this->title."' WHERE `book`.`id` = ".$this->id;
        $result=mysqli_query($this->conn, $query);
        if($result){
            //echo "Data Has been stored successfully";
            Message::message("Data has been stored successfully");
            Utility::redirect("index.php");

        }
        else {
            echo "Error";
        }
    }
    public function delete(){
        $query="DELETE FROM `atomicprojectsarah`.`book` WHERE `book`.`id` = ".$this->id;
        $result=mysqli_query($this->conn, $query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
  <strong>Deleted!</strong> Data has been deleted successfully.
  </div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been deleted successfully.
    </div>");
            Utility::redirect('index.php');

        }
    }
    public function trash(){
        $this->deleted_at=time();
        $query="UPDATE `atomicprojectsarah`.`book` SET `deleted_at` = '".$this->deleted_at."' WHERE `book`.`id` = " .$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
  <strong>Trashed!</strong> Data has been trashed successfully.
</div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been trashed successfully.
    </div>");
            Utility::redirect('index.php');

        }
    }
    public function trashed(){
        $_trashedBook=array();
        $query="SELECT * FROM `book`WHERE `deleted_at` IS NOT NULL";
        $result= mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result)){
            $_trashedBook[]=$row;
        }

        return $_trashedBook;


    }

    public function recover(){
        $query="UPDATE `atomicprojectsarah`.`book` SET `deleted_at` = NULL WHERE `book`.`id` = " .$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
  <strong>Trashed!</strong> Data has been trashed successfully.
</div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been trashed successfully.
    </div>");
            Utility::redirect('index.php');

        }

    }

    public function recoverMultiple($idS=array()){
        if((is_array($idS)) && count($idS)>0){
            $IDs= implode(",",$idS);
            $query="UPDATE `atomicprojectsarah`.`book` SET `deleted_at` = NULL  WHERE `book`.`id` IN(".$IDs.")";
            $result= mysqli_query($this->conn,$query);
            if($result){
                Message::message("<div class=\"alert alert-success\">
  <strong>Recovered!</strong> Selected Data has been recovered successfully.
</div>");
                header('Location:index.php');

            } else {
                Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Selected Data has not been recovered successfully.
    </div>");
                Utility::redirect('index.php');

            }
        }

    }
    public function deleteMultiple($idS=array()){
        if((is_array($idS)) && count($idS)>0){
            $IDs= implode(",",$idS);
            $query="DELETE FROM `atomicprojectsarah`.`book`  WHERE `book`.`id` IN(".$IDs.")";
            $result= mysqli_query($this->conn,$query);
            if($result){
                Message::message("<div class=\"alert alert-success\">
  <strong>Recovered!</strong> Selected Data has been deleted successfully.
</div>");
                header('Location:index.php');

            } else {
                Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Selected Data has not been deleted successfully.
    </div>");
                Utility::redirect('index.php');

            }
        }

    }

    public function count(){
        $query="SELECT COUNT(*) AS totalItem FROM `atomicprojectsarah`.`book`  WHERE `deleted_at` IS NULL  ";
        $result=mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row['totalItem'];
    }

    public function paginator($pageStartFrom=0,$Limit=5){
        $_allBook = array();
        $query="SELECT * FROM `book` WHERE deleted_at IS NULL LIMIT ".$pageStartFrom.",".$Limit;
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_assoc($result)) {
            $_allBook[] = $row;
        }
        return $_allBook;

    }

    public function getAllTitle()
    {
        $_allBook = array();
        $query = "SELECT `title` FROM `book` WHERE `deleted_at` IS NULL";
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_assoc($result)) {
            $_allBook[] = $row['title'];
        }

        return $_allBook;


    }

    public function getAllDesc()
    {
        $_allBook = array();
        $query = "SELECT `strippedDesc` FROM `book` WHERE `deleted_at` IS NULL";
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_assoc($result)) {
            $_allBook[] = $row['strippedDesc'];
        }

        return $_allBook;


    }

}


