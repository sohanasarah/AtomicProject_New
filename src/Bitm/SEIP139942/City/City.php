<?php
namespace App\Bitm\SEIP139942\City;
use App\Bitm\SEIP139942\Message\Message;
use App\Bitm\SEIP139942\Utility\Utility;
class City{
    public $id;
    public $city;
    public $conn;
    public $description="";
    public $filterByTitle="";
    public $filterByDescription="";
    public $search="";

    public function __construct()
    {
        $this->conn=mysqli_connect("localhost","root","","atomicprojectsarah") or die ("Database connection failed");
    }

    public function prepare($data="")
    {
        if(array_key_exists("city",$data))
        {
            $this->city=$data['city'];
        }
        if(array_key_exists("id",$data))
        {
            $this->id=$data['id'];
        }
        if (array_key_exists("description", $data)) {
            $this->description = $data['description'];
        }
        if (array_key_exists("description", $data)) {
            $this->strippedDesc = $data['strippedDesc'];
        }

        if (array_key_exists("filterByTitle", $data)) {
            $this->filterByTitle = $data['filterByTitle'];
        }
        if (array_key_exists("filterByDescription", $data)) {
            $this->filterByDescription = $data['filterByDescription'];
        }
        if (array_key_exists("search", $data)) {
            $this->search = $data['search'];
        }
        return $this;
    }

    public function store()
    {
        //$query="INSERT INTO `atomicprojectsarah`.`city` (`city`) VALUES ('".$this->city."')";
        $query="INSERT INTO `atomicprojectsarah`.`city` ( `city`, `description`, `strippedDesc`) VALUES ('".$this->city."', '".$this->description."', '".$this->strippedDesc."')";

        $result=mysqli_query($this->conn,$query);
        if($result)
        {
            Message::message("
        <div class=\"alert alert-success\">
        <strong>Success!</strong> Data has been stored successfully.
        </div>");
            Utility::redirect("index.php");
        }
        else{
            echo "Error!!!";
        }
    }


    public function index()
    {
        $whereClause= " 1=1 ";
        if(!empty($this->filterByTitle)){
            $whereClause.=" AND  city LIKE '%".$this->filterByTitle."%'";
        }
        if(!empty($this->filterByDescription)){
            $whereClause.=" AND  description LIKE '%".$this->filterByDescription."%'";
        }
        if(!empty($this->search)){
            $whereClause.=" AND  description LIKE '%".$this->search."%' OR title LIKE '%".$this->search."%'";
        }
        $_cities=array();
        $query="SELECT * FROM `city` WHERE `deleted_at` IS NULL AND ".$whereClause;
        $result=mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result))
        {
            $_cities[]=$row;
        }
        return $_cities;
    }

    public function view()
    {
        $query="SELECT * FROM `city` WHERE id=".$this->id;
        $result=mysqli_query($this->conn,$query);
        $row=mysqli_fetch_assoc($result);
        return $row;
    }

    public function update()
    {
        $query = "UPDATE `atomicprojectsarah`.`city` SET `city` = '".$this->city."' WHERE `city`.`id` =".$this->id;
        $result = mysqli_query($this->conn,$query);
        if($result)
        {
            Message::message("
            <div class=\"alert alert-info\">
            <strong>Updated!</strong> Data has been updated.
            </div>");
            Utility::redirect('index.php');
        }
        else
        {
            echo "Error!";
        }
    }

    public function delete()
    {
        $query="DELETE FROM `atomicprojectsarah`.`city` WHERE `city`.`id`=".$this->id;
        $result = mysqli_query($this->conn,$query);
        if($result)
        {
            Message::message("
        <div class=\"alert alert-success\">
        <strong>Success!</strong> Data has been deleted successfully.
        </div>");
            Utility::redirect("index.php");
        }
        else{
            echo "Error!";
        }
    }
    public function trash(){
        $this->deleted_at=time();
        $query="UPDATE `atomicprojectsarah`.`city` SET `deleted_at` = '".$this->deleted_at."' WHERE `city`.`id` = " .$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
  <strong>Trashed!</strong> Data has been trashed successfully.
</div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been trashed successfully.
    </div>");
            Utility::redirect('index.php');

        }
    }
    public function trashed(){
        $_trashed=array();
        $query="SELECT * FROM `city` WHERE `deleted_at` IS NOT NULL";
        $result= mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result)){
            $_trashed[]=$row;
        }

        return $_trashed;


    }
    public function recover(){
        $query="UPDATE `atomicprojectsarah`.`city` SET `deleted_at` = NULL WHERE `city`.`id` = " .$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
  <strong>Trashed!</strong> Data has been trashed successfully.
</div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been trashed successfully.
    </div>");
            Utility::redirect('index.php');

        }

    }
    public function recoverMultiple($idS=array()){
        if((is_array($idS)) && count($idS)>0){
            $IDs= implode(",",$idS);
            $query="UPDATE `atomicprojectsarah`.`city` SET `deleted_at` = NULL  WHERE `city`.`id` IN(".$IDs.")";
            $result= mysqli_query($this->conn,$query);
            if($result){
                Message::message("<div class=\"alert alert-success\">
  <strong>Recovered!</strong> Selected Data has been recovered successfully.
</div>");
                header('Location:index.php');

            } else {
                Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Selected Data has not been recovered successfully.
    </div>");
                Utility::redirect('index.php');

            }
        }

    }
    public function deleteMultiple($idS=array()){
        if((is_array($idS)) && count($idS)>0){
            $IDs= implode(",",$idS);
            $query="DELETE FROM `atomicprojectsarah`.`city`  WHERE `city`.`id` IN(".$IDs.")";
            $result= mysqli_query($this->conn,$query);
            if($result){
                Message::message("<div class=\"alert alert-success\">
  <strong>Recovered!</strong> Selected Data has been deleted successfully.
</div>");
                header('Location:index.php');

            } else {
                Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Selected Data has not been deleted successfully.
    </div>");
                Utility::redirect('index.php');

            }
        }

    }
    public function count(){
        $query="SELECT COUNT(*) AS totalItem FROM `atomicprojectsarah`.`city`  WHERE `deleted_at` IS NULL  ";

        $result=mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row['totalItem'];
    }

    public function paginator($pageStartFrom=0,$Limit=5){
        $_allCity = array();
        $query="SELECT * FROM `city` WHERE deleted_at IS NULL LIMIT ".$pageStartFrom.",".$Limit;

        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_assoc($result)) {
            $_allCity[] = $row;
        }
        return $_allCity;

    }
    public function getAllCity()
    {
        $_allCity = array();
        $query = "SELECT `city` FROM `city` WHERE `deleted_at` IS NULL";
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_assoc($result)) {
            $_allCity[] = $row['city'];
        }

        return $_allCity;


    }

    public function getAllDesc()
    {
        $_allCity = array();
        $query = "SELECT `strippedDesc` FROM `city` WHERE `deleted_at` IS NULL";
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_assoc($result)) {
            $_allCity[] = $row['strippedDesc'];
        }

        return $_allCity;


    }



}