<?php
namespace App\Bitm\SEIP139942\Summary;
use App\Bitm\SEIP139942\Message\Message;
use App\Bitm\SEIP139942\Utility\Utility;
class Summary{
    public $id;
    public $org;
    public $summary;
    public $conn;

    public function __construct()
    {
        $this->conn=mysqli_connect("localhost","root","","atomicprojectsarah") or die ("Database connection failed");
    }

    public function prepare($data="")
    {
        if(array_key_exists("summary",$data))
        {
            $this->summary=$data['summary'];
        }
        if(array_key_exists("org",$data))
        {
            $this->org=$data['org'];
        }
        if(array_key_exists("id",$data))
        {
            $this->id=$data['id'];
        }
    }

    public function store()
    {
        $query="INSERT INTO `summary` (`organization`, `summary`) VALUES ( '".$this->org."', '".$this->summary."')";
        $result=mysqli_query($this->conn,$query);
        if($result)
        {
            Message::message("
        <div class=\"alert alert-success\">
        <strong>Success!</strong> Data has been stored successfully.
        </div>");
            Utility::redirect("index.php");
        }
        else{
            echo "Error!!!";
        }
    }


    public function index()
    {
        $_sum=array();
        $query="SELECT * FROM `summary` WHERE `deleted_at` IS NULL";
        $result=mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result))
        {
            $_sum[]=$row;
        }
        return $_sum;
    }

    public function view()
    {
        $query="SELECT * FROM `summary` WHERE id=".$this->id;
        $result=mysqli_query($this->conn,$query);
        $row=mysqli_fetch_assoc($result);
        return $row;
    }

    public function update()
    {
        if(!empty($this->org)) {
            $query = "UPDATE `atomicprojectsarah`.`summary` SET `organization` = '" . $this->org . "', `summary` = '" . $this->summary . "' WHERE `summary`.`id` =" . $this->id;

        }
        else{
            $query = "UPDATE `atomicprojectsarah`.`summary` SET `summary` = '".$this->summary."' WHERE `summary`.`id` =".$this->id;

        }

        $result = mysqli_query($this->conn,$query);
        if($result)
        {
            Message::message("
            <div class=\"alert alert-info\">
            <strong>Updated!</strong> Data has been updated.
            </div>");
            Utility::redirect('index.php');
        }
        else
        {
            echo "Error!";
            Utility::redirect('index.php');
        }
    }

    public function delete()
    {
        $query="DELETE FROM `atomicprojectsarah`.`summary` WHERE `summary`.`id`=".$this->id;
        $result = mysqli_query($this->conn,$query);
        if($result)
        {
            Message::message("
        <div class=\"alert alert-success\">
        <strong>Success!</strong> Data has been deleted successfully.
        </div>");
            Utility::redirect("index.php");
        }
        else{
            echo "Error!";
        }
    }
    public function trash(){
        $this->deleted_at=time();
        $query="UPDATE `atomicprojectsarah`.`summary` SET `deleted_at` = '".$this->deleted_at."' WHERE `summary`.`id` = " .$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
  <strong>Trashed!</strong> Data has been trashed successfully.
</div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been trashed successfully.
    </div>");
            Utility::redirect('index.php');

        }
    }
    public function trashed(){
        $_trashed=array();
        $query="SELECT * FROM `summary` WHERE `deleted_at` IS NOT NULL";
        $result= mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result)){
            $_trashed[]=$row;
        }

        return $_trashed;


    }
    public function recover(){
        $query="UPDATE `atomicprojectsarah`.`summary` SET `deleted_at` = NULL WHERE `summary`.`id` = " .$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
  <strong>Trashed!</strong> Data has been trashed successfully.
</div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been trashed successfully.
    </div>");
            Utility::redirect('index.php');

        }

    }
    public function recoverMultiple($idS=array()){
        if((is_array($idS)) && count($idS)>0){
            $IDs= implode(",",$idS);
            $query="UPDATE `atomicprojectsarah`.`summary` SET `deleted_at` = NULL  WHERE `summary`.`id` IN(".$IDs.")";
            $result= mysqli_query($this->conn,$query);
            if($result){
                Message::message("<div class=\"alert alert-success\">
  <strong>Recovered!</strong> Selected Data has been recovered successfully.
</div>");
                header('Location:index.php');

            } else {
                Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Selected Data has not been recovered successfully.
    </div>");
                Utility::redirect('index.php');

            }
        }

    }
    public function deleteMultiple($idS=array()){
        if((is_array($idS)) && count($idS)>0){
            $IDs= implode(",",$idS);
            $query="DELETE FROM `atomicprojectsarah`.`summary`  WHERE `summary`.`id` IN(".$IDs.")";
            $result= mysqli_query($this->conn,$query);
            if($result){
                Message::message("<div class=\"alert alert-success\">
  <strong>Recovered!</strong> Selected Data has been deleted successfully.
</div>");
                header('Location:index.php');

            } else {
                Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Selected Data has not been deleted successfully.
    </div>");
                Utility::redirect('index.php');

            }
        }

    }

    public function count(){
        $query="SELECT COUNT(*) AS totalItem FROM `atomicprojectsarah`.`summary` ";
        $result=mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row['totalItem'];
    }

    public function paginator($pageStartFrom=0,$Limit=5){
        $_allOrg = array();
        $query="SELECT * FROM `summary` WHERE deleted_at IS NULL LIMIT ".$pageStartFrom.",".$Limit;
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_assoc($result)) {
            $_allOrg[] = $row;
        }
        return $_allOrg;

    }



}