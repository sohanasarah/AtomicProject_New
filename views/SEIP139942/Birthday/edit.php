<?php
include_once "../../../vendor/autoload.php";
use App\Bitm\SEIP139942\Birthday\Birthday;
use App\Bitm\SEIP139942\Utility\Utility;

$day=new Birthday();
$day->prepare($_GET);
$singleItem=$day->view();
//Utility::d($singleItem);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Birthday</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2><b>Edit Birthday</b></h2>
    <form role="form" action="update.php" method="post">
        <div class="form-group">
            <input type="hidden" name="id" id="bday" value="<?php echo $singleItem['id'] ?>">
            <label>Edit Your Birthday (mm/dd/yyyy): </label>
            <input type="date" name="bday" class="form-control" value="<?php echo $singleItem['b_day']?>">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>

</body>
</html>
