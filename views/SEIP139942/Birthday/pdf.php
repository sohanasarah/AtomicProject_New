<?php
require_once('../../../vendor/mpdf/mpdf/mpdf.php');
include_once('../../../vendor/autoload.php');
use \App\Bitm\SEIP139942\Birthday\Birthday;
$obj= new Birthday();
$allData= $obj->index();
$trs="";
$sl=0;
foreach($allData as $data):
    $sl++;
    $trs.="<tr>";
    $trs.="<td>$sl </td>";
    $trs.="<td>".$data['id']."</td>";
    $trs.="<td>".$data['b_day']."</td>";
    $trs.="</tr>";
endforeach;
$html=<<<EOD
<table class="table">
            <thead>
            <tr>
                <th>SL </th>
                <th>ID </th>
                <th>Birthday</th>

           </tr>
            </thead>
            <tbody>
                 $trs
            </tbody>
</table>
EOD;

$mpdf = new mPDF();

// Write some HTML code:

$mpdf->WriteHTML($html);

// Output a PDF file directly to the browser
$mpdf->Output('mylist.pdf','D');