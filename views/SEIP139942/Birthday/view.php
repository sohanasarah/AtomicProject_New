<?php
include_once "../../../vendor/autoload.php";
use App\Bitm\SEIP139942\Birthday\Birthday;
use App\Bitm\SEIP139942\Utility\Utility;

$day=new Birthday();
$day->prepare($_GET);
$singleItem=$day->view();
//Utility::dd($singleItem);
$_bday=$singleItem['b_day'];
$_bday=explode("-", $_bday);
$birthday="$_bday[2]-$_bday[1]-$_bday[0]";

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>View Birthday</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2><b>View Birthday</b></h2>
    <ul class="list-group">
        <li class="list-group-item list-group-item-info"><b>ID:</b>   <?php echo $singleItem['id'] ?></li>
        <li class="list-group-item list-group-item-info"><b>Birthday:</b>   <?php
            echo $birthday;
            //echo $singleItem['b_day'];
//            $_bday=$singleItem['b_day'];
//            $_bday=explode("-", $_bday);
//            $birthday="$_bday[2]-$_bday[1]-$_bday[0]";
//            echo $birthday;
            ?></li>
    </ul>
</div>

</body>
</html>