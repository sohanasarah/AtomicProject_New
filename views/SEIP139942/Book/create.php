<!DOCTYPE html>
<html lang="en">
<head>
  <title>Book Title</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

  <script src='//cdn.tinymce.com/4/tinymce.min.js'></script>
</head>
<body>

<div class="container">
  <h2><b>BOOK TITLE</b></h2>
  <form role="form" action="store.php" method="post">
    <div class="form-group">
      <label>Enter Book title: </label>
      <input type="text" name="title" class="form-control" id="book" placeholder="Enter booktitle">
    </div>
    <div class="form-group">
      <label>Description:</label>
      <textarea class="form-control" rows="5" id="mytextarea" name="description"></textarea>
    </div>
    <button type="submit" class="btn btn-default">Submit</button>
  </form>
</div>
<script>
  tinymce.init({
    selector: '#mytextarea'
  });
</script>

</body>
</html>
