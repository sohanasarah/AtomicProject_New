<?php
session_start();
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP139942\Book\Book;
use App\Bitm\SEIP139942\Utility\Utility;
use App\Bitm\SEIP139942\Message\Message;

$book= new Book();
$availableTitle=$book->getAllTitle();

$comma_separated= '"'.implode('","',$availableTitle).'"';
$availableDescription=$book->getAllDesc();
$comma_separated2= '"'.implode('","',$availableDescription).'"';

//$allBook=$book->index();
//Utility::d($allBook);
if(array_key_exists('itemPerPage',$_SESSION)) {
    if (array_key_exists('itemPerPage', $_GET)) {
        $_SESSION['itemPerPage'] = $_GET['itemPerPage'];
    }
}
else{
    $_SESSION['itemPerPage']=5;
}
$itemPerPage=$_SESSION['itemPerPage'];
$totalItem=$book->count();
$totalPage=ceil($totalItem/$itemPerPage);
//Utility::dd($itemPerPage);
$pagination="";
if(array_key_exists('pageNumber',$_GET)){
    $pageNumber=$_GET['pageNumber'];
}else{
    $pageNumber=1;
}
for($i=1;$i<=$totalPage;$i++){
    $class=($pageNumber==$i)?"active":"";
    $pagination.="<li class='$class'><a href='index.php?pageNumber=$i'>$i</a></li>";
}

$pageStartFrom=$itemPerPage*($pageNumber-1);

$prev=$pageNumber-1;
$next=$pageNumber+1;
$previous="<li><a href='index.php?pageNumber=$prev'>Prev</a></li>";
$next="<li><a href='index.php?pageNumber=$next'>Next</a></li>";
if(strtoupper($_SERVER['REQUEST_METHOD']=='GET')) {
    $allBook = $book->paginator($pageStartFrom, $itemPerPage);
}
if(strtoupper($_SERVER['REQUEST_METHOD']=='POST')) {
    $allBook = $book->prepare($_POST)->index();
}
if((strtoupper($_SERVER['REQUEST_METHOD']=='GET'))&& isset($_GET['search'])) {
    $allBook = $book->prepare($_GET)->index();
}

?>

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>

</head>
<body>

<div class="container">
    <h2>All Book List</h2>
    <a href="create.php" class="btn btn-primary btn-lg" role="button">Create again</a>
    <a href="trashed_view.php" class="btn btn-primary btn-lg" role="button">Trashed List</a><br><br>
    <a href="pdf.php" class="btn btn-info btn-lg" role="button">Download as PDF</a>
    <a href="excel.php" class="btn btn-success btn-lg" role="button">Download as Excel</a>
    <a href="email.php" class="btn btn-primary btn-lg" role="button">Email to Friend</a>
    <br>
    <div id="message">
        <?php if((array_key_exists('message',$_SESSION)&& (!empty($_SESSION['message'])))) {
            echo Message::message();
        }
        ?>
    </div>
    <form role="form">
        <div class="form-group">
            <label for="sel1">Select items you want to show:</label>
            <select class="form-control" id="sel1" name="itemPerPage">
                <option <?php if($itemPerPage==5){?> selected="selected" <?php } ?> >5</option>
                <option <?php if($itemPerPage==10){?> selected="selected" <?php } ?>>10</option>
                <option <?php if($itemPerPage==15){?> selected="selected" <?php } ?>>15</option>
                <option <?php if($itemPerPage==20){?> selected="selected" <?php } ?>>20</option>
                <option <?php if($itemPerPage==25){?> selected="selected" <?php } ?>>25</option>
            </select>
            <button type="submit" class="btn btn-primary btn-sm">Go</button>

        </div>
    </form>
    <form action="index.php" method="post">
        <label>Filter by Title</label>
        <input type="text" name="filterByTitle" value="" id="title">
        <label>Filter by Description</label>
        <input type="text" name="filterByDescription" value="" id="desc">
        <button type="submit">Submit</button>
    </form>
    <form action="index.php" method="get">
        <label>Search</label>
        <input type="text" name="search" value="">
        <button type="submit">Search</button>
    </form>
    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th>#</th>
                <th>ID</th>
                <th>Book title</th>
                <th>Description</th>
                <th>Action</th>

            </tr>
            </thead>
            <tbody>
            <tr>
                <?php
                $sl=0;
                foreach($allBook as $book){
                $sl++; ?>
                <td><?php echo $sl+$pageStartFrom?></td>
                <td><?php echo $book['id'] ?></td>
                <td><?php echo $book['title']?></td>
                <td><?php echo $book['description']?></td>
                <td><a href="view.php?id=<?php echo $book['id'] ?>" class="btn btn-primary" role="button">View</a>
                    <a href="edit.php?id=<?php echo $book['id'] ?>"  class="btn btn-info" role="button">Edit</a>
                    <a href="delete.php?id=<?php echo $book['id'] ?>" class="btn btn-danger" role="button">Delete</a>
                    <a href="trash.php?id=<?php echo $book['id'] ?>" class="btn btn-primary" role="button">Trash</a>
                </td>

            </tr>
            <?php }?>


            </tbody>
        </table>
    </div>
    <?php if(strtoupper($_SERVER['REQUEST_METHOD']=='GET')) { ?>
    <div class="text-center">

        <ul class="pagination">
            <?php if($pageNumber>1) echo $previous?>
            <?php echo $pagination?>
            <?php if($pageNumber!=$totalPage) echo $next?>
        </ul>

    </div>
    <?php } ?>

</div>
<script>
    $('#message').show().delay(2000).fadeOut()
</script>
<script>
    $( function() {
        var availableTags = [
            <?php echo $comma_separated?>
        ];
        $( "#title" ).autocomplete({
            source: availableTags
        });
    } );
</script>
<script>
    $( function() {
        var availableTags = [
            <?php echo $comma_separated2?>
        ];
        $( "#desc" ).autocomplete({
            source: availableTags
        });
    } );
</script>

</body>
</html>
