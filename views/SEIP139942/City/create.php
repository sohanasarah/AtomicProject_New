<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src='//cdn.tinymce.com/4/tinymce.min.js'></script>
</head>
<body>

<div class="container">
    <h2>Select Your City</h2>
    <form role="form" action="store.php" method="post">
        <div class="form-group">
            <label for="sel1">Select One:</label>
            <select class="form-control" id="sel1" name="city">
                <option>Chittagong</option>
                <option>Dhaka</option>
                <option>Sylhet</option>
                <option>Barisal</option>
                <option>Comilla</option>
                <option>Rajshahi</option>
                <option>Khulna</option>
                <option>Jessore</option>
                <option>Pabna</option>
                <option>Rangpur</option>
            </select>
        </div>
        <div class="form-group">
            <label>Description:</label>
            <textarea class="form-control" rows="5" id="mytextarea" name="description"></textarea>
        </div>
        <button type="submit" class="btn btn-info">Submit</button>
    </form>
</div>
<script>
    tinymce.init({
        selector: '#mytextarea'
    });
</script>

</body>
</html>

