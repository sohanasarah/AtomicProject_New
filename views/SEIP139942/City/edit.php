<?php
include_once ('../../../vendor/autoload.php');
use App\Bitm\SEIP139942\City\City;
use App\Bitm\SEIP139942\Utility\Utility;
use App\Bitm\SEIP139942\Message\Message;

$city= new City();
$city->prepare($_GET);
$singleItem=$city->view();
//Utility::d($singleItem);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>City</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Edit Your City</h2>
    <form role="form" method="post" action="update.php">
        <input type="hidden" name="id" id="city" value="<?php echo $singleItem['id']?>">
        <div class="form-group">
            <label for="sel1">Select One:</label>
            <select class="form-control" id="sel1" name="city">
                <option <?php if($singleItem['city']=='Chittagong'){?> selected="selected" <?php }?>>Chittagong</option>
                <option  <?php if($singleItem['city']=='Dhaka'){?> selected="selected" <?php }?>>Dhaka</option>
                <option  <?php if($singleItem['city']=='Sylhet'){?> selected="selected" <?php }?>>Sylhet</option>
                <option  <?php if($singleItem['city']=='Barisal'){?> selected="selected" <?php }?>>Barisal</option>
                <option  <?php if($singleItem['city']=='Comilla'){?> selected="selected" <?php }?>>Comilla</option>
                <option  <?php if($singleItem['city']=='Rajshahi'){?> selected="selected" <?php }?>>Rajshahi</option>
                <option <?php if($singleItem['city']=='Khulna'){?> selected="selected" <?php }?>>Khulna</option>
                <option  <?php if($singleItem['city']=='Jessore'){?> selected="selected" <?php }?>>Jessore</option>
                <option  <?php if($singleItem['city']=='Pabna'){?> selected="selected" <?php }?>>Pabna</option>
                <option  <?php if($singleItem['city']=='Rangpur'){?> selected="selected" <?php }?>>Rangpur</option>

            </select>
        </div>
        <div class="form-group">
            <label>Description:</label>
            <textarea class="form-control" rows="5" id="mytextarea" name="description" value="<?php echo $singleItem['title']?>"></textarea>
        </div>
        <button type="submit" class="btn btn-default">Update</button>
    </form>
</div>

</body>
</html>