<?php
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP139942\City\City;
use App\Bitm\SEIP139942\Utility\Utility;

$city= new City();
$trashedCity=$city->trashed();
//Utility::d($allBook);

?>
<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>All Trashed List</h2>
    <a href="index.php" class="btn btn-primary btn-lg" role="button">View City List</a>
    <br><br>
    <form action="recoverMultiple.php" method="post" id="multiple">
        <button type="submit" class="btn btn-primary btn-lg">Recover Selected</button>
        <button type="button" class="btn btn-danger btn-lg" id="delete">Delete all Selected</button>
        <br><br>

        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th>SELECT</th>
                    <th>#</th>
                    <th>ID</th>
                    <th>City</th>
                    <th>Action</th>

                </tr>
                </thead>
                <tbody>
                <tr>
                    <?php
                    $sl=0;
                    foreach($trashedCity as $city){
                    $sl++; ?>
                    <td><input type="checkbox" name=mark[] value="<?php echo $city['id'] ?>"></td>
                    <td><?php echo $sl?></td>
                    <td><?php echo $city['id']?></td>
                    <td><?php echo $city['city']?></td>
                    <td><a href="recover.php?id=<?php echo $city['id'] ?>" class="btn btn-primary" role="button">Recover</a>
                        <a href="delete.php?id=<?php echo $city['id'] ?>"  class="btn btn-danger" role="button">Delete</a>
                    </td>

                </tr>
                <?php }?>


                </tbody>
            </table>
    </form>
</div>
</div>
<script>
    $('#delete').on('click',function(){
        document.forms[0].action="deleteMultiple.php";
        $('#multiple').submit();
    });
</script>

</body>
</html>