<?php
include_once "../../../vendor/autoload.php";
use App\Bitm\SEIP139942\City\City;

$city=new City();
$city->prepare($_GET);
$singleItem=$city->view();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>View City</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2><b>View City</b></h2>
    <br>
    <ul class="list-group">
        <li class="list-group-item list-group-item-info"><b>ID:</b> <?php echo $singleItem['id'] ?></li>
        <li class="list-group-item list-group-item-info"><b>City:</b>  <?php echo $singleItem['city']?></li>
    </ul>
</div>

</body>
</html>