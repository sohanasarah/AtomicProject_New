<!DOCTYPE html>
<html lang="en">
<head>
    <title>Atomic Project</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../Resources/bootstrap/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Select User Name and Gender</h2>
    <form role="form" action="store.php" method="post">
        <div class="form-group">
            <label>Enter Name:</label>
            <input type="text" name="user" class="form-control">
        </div>
        <div class="form-group">
            <div class="radio">
                <label><input type="radio" name="gender" value="Female">Female</label>
            </div>
            <div class="radio">
                <label><input type="radio" name="gender" value="Male">Male</label>
            </div>
            <div class="radio">
                <label><input type="radio" name="gender" value="Other">Other</label>
            </div>

        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>

</body>
</html>