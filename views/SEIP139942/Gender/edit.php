<?php
include_once "../../../vendor/autoload.php";
use App\Bitm\SEIP139942\Gender\Gender;

$user=new Gender();
$user->prepare($_GET);
$singleItem=$user->view();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Atomic Project</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../Resources/bootstrap/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Select User Name and Gender</h2>
    <form role="form" action="update.php" method="post">
        <div class="form-group">
            <input type="hidden" name="id" id="gender" value="<?php echo $singleItem['id']?>">
            <label>Enter Name:</label>
            <input type="text" name="user" class="form-control" value="<?php echo $singleItem['user']?>">
        </div>
        <div class="form-group">
            <div class="radio">
                <label><input type="radio" name="gender" value="Female" <?php if($singleItem['gender']=="Female"){
                        echo "checked";
                    }?>>Female</label>
            </div>
            <div class="radio">
                <label><input type="radio" name="gender" value="Male" <?php if($singleItem['gender']=="Male"){
                        echo "checked";
                    }?>>Male</label>
            </div>
            <div class="radio">
                <label><input type="radio" name="gender" value="Male" <?php if($singleItem['gender']=="Other"){
                        echo "checked";
                    }?>>Other</label>
            </div>

        </div>

        <button type="submit" class="btn btn-primary">Update</button>
    </form>
</div>
