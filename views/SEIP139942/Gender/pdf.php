<?php
require_once('../../../vendor/mpdf/mpdf/mpdf.php');
include_once('../../../vendor/autoload.php');
use \App\Bitm\SEIP139942\Gender\Gender;
$obj= new Gender();
$allData= $obj->index();
$trs="";
$sl=0;
foreach($allData as $data):
    $sl++;
    $trs.="<tr>";
    $trs.="<td>$sl </td>";
    $trs.="<td>".$data['id']."</td>";
    $trs.="<td>".$data['user']."</td>";
    $trs.="<td>".$data['gender']."</td>";
    $trs.="</tr>";
endforeach;
$html=<<<EOD
<table class="table">
            <thead>
            <tr>
                <th>SL </th>
                <th>ID </th>
                <th>User </th>
                <th>Gender </th>

           </tr>
            </thead>
            <tbody>
                 $trs
            </tbody>
</table>
EOD;

$mpdf = new mPDF();

// Write some HTML code:

$mpdf->WriteHTML($html);

// Output a PDF file directly to the browser
$mpdf->Output('gender.pdf','D');