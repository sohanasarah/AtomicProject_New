<?php
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP139942\Gender\Gender;
use App\Bitm\SEIP139942\Utility\Utility;
use App\Bitm\SEIP139942\Message\Message;

$user=new Gender();
$trashedUser=$user->trashed();

?>

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../Resources/bootstrap/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Trash List</h2>
    <a href="index.php" class="btn btn-info btn-lg" role="button">Back to Index</a>

    <br> <br>
    <form action="recoverMultiple.php" method="post" id="multiple">
        <button type="submit" class="btn btn-info btn-lg">Recover all Selected</button>
        <button type="button" class="btn btn-danger btn-lg" id="delete">Delete all Selected</button>
        <br><br>
        <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th>Select</th>
                <th>#</th>
                <th>ID</th>
                <th>User</th>
                <th>Gender</th>
                <th>Action</th>

            </tr>
            </thead>
            <tbody>
            <tr>
                <?php
                $sl=0;
                foreach($trashedUser as $user){
                $sl++;
                ?>
                <td><input type="checkbox" name=mark[] value="<?php echo $user['id'] ?>"></td>
                <td><?php echo $sl?></td>
                <td><?php echo $user['id']?></td>
                <td><?php echo $user['user']?></td>
                <td><?php echo $user['gender']?></td>
                <td>
                    <a href="delete.php?id=<?php echo $user['id']?>" class="btn btn-danger" role="button">Delete</a>
                    <a href="recover.php?id=<?php echo $user['id']?>" class="btn btn-info" role="button">Recover</a>
                </td>


            </tr>
            <?php } ?>
            </tbody>
        </table>
    </form>
    </div>
</div>

<script>
    $('#delete').on('click',function(){
        document.forms[0].action="deleteMultiple.php";
        $('#multiple').submit();
    });
</script>

</body>
</html>