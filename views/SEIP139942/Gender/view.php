<?php
include_once "../../../vendor/autoload.php";
use App\Bitm\SEIP139942\Gender\Gender;

$user=new Gender();
$user->prepare($_GET);
$singleItem=$user->view();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>View</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../Resources/bootstrap/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2><b>View User's Gender</b></h2>
    <ul class="list-group">
        <li class="list-group-item list-group-item-info"><b>ID:</b> <?php echo $singleItem['id'] ?></li>
        <li class="list-group-item list-group-item-info"><b>Name:</b> <?php echo $singleItem['user'] ?></li>
        <li class="list-group-item list-group-item-info"><b>Gender:</b> <?php echo $singleItem['gender']?></li>
    </ul>
</div>

</body>
</html>