<!DOCTYPE html>
<html lang="en">
<head>
    <title>Hobby</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2><b>Choose Your Hobbies</b></h2>
    <form role="form" action="store.php" method="post">
        <div class="container">
            <div class="checkbox">
                <label><input type="checkbox" name="hobby[]" value="Gardening">Gardening</label>
            </div>
            <div class="checkbox">
                <label><input type="checkbox" name="hobby[]" value="Coding">Coding</label>
            </div>
            <div class="checkbox">
                <label><input type="checkbox" name="hobby[]" value="Reading">Reading</label>
            </div>
            <div class="checkbox">
                <label><input type="checkbox" name="hobby[]" value="Football">Football</label>
            </div>
            <div class="checkbox">
                <label><input type="checkbox" name="hobby[]" value="Cricket">Cricket</label>
            </div>
            <div class="checkbox">
                <label><input type="checkbox" name="hobby[]" value="Writing">Writing</label>
            </div>
            <div class="checkbox">
                <label><input type="checkbox" name="hobby[]" value="Watching TV">Watching TV</label>
            </div>
            <div class="checkbox">
                <label><input type="checkbox" name="hobby[]" value="Travelling">Travelling</label>
            </div>
            <div class="checkbox">
                <label><input type="checkbox" name="hobby[]" value="Swimming">Swimming</label>
            </div>
            <div class="checkbox">
                <label><input type="checkbox" name="hobby[]" value="Sleeping">Sleeping</label>
            </div>
            <div class="checkbox">
                <label><input type="checkbox" name="hobby[]" value="Cooking">Cooking</label>
            </div>

        </div>
        <br>
        <button type="submit" class="btn btn-info">Submit</button>
    </form>
</div>

</body>
</html>