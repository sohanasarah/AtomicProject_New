<?php
include_once('../../../vendor/autoload.php');

use App\Bitm\SEIP139942\Hobby\Hobby;
use App\Bitm\SEIP139942\Utility\Utility;

$hobby=new Hobby();
$hobby->prepare($_GET);
$singleItem=$hobby->view();
//Utility::d($singleItem);
$_hobby=explode(",",$singleItem['hobbies']);
//Utility::d($_hobby);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Edit Hobby</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>


<body>

<div class="container">
    <h2>Edit hobbies</h2>
    <br><br>
    <form role="form" method="post" action="update.php">
        <input type="hidden" name="id" id="title" value="<?php echo $singleItem['id']?>">

        <div class="checkbox">
            <label><input type="checkbox" name=hobby[] value="Gardening" <?php if(in_array("Gardening",$_hobby))
                {
                    echo "checked";
                } ?>>Gardening</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name=hobby[] value="Coding" <?php if(in_array("Coding",$_hobby))
                {
                    echo "checked";
                } ?>>Coding</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name=hobby[] value="Reading"<?php if(in_array("Reading",$_hobby))
                {
                    echo "checked";
                } ?>>Reading</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name=hobby[] value="Football"<?php if(in_array("Football",$_hobby))
                {
                    echo "checked";
                } ?>>Football</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="hobby[]" value="Cricket"<?php if(in_array("Cricket",$_hobby)){
                    echo "checked";
                }?>>Cricket</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="hobby[]" value="Writing"<?php if(in_array("Writing",$_hobby)){
                    echo "checked";
                }?>>Writing</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="hobby[]" value="Watching TV"<?php if(in_array("Watching TV",$_hobby)){
                    echo "checked";
                }?>>Watching TV</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="hobby[]" value="Travelling"<?php if(in_array("Travelling",$_hobby)){
                    echo "checked";
                }?>>Travelling</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="hobby[]" value="Swimming"<?php if(in_array("Swimming",$_hobby)){
                    echo "checked";
                }?>>Swimming</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="hobby[]" value="Sleeping"<?php if(in_array("Sleeping",$_hobby)){
                    echo "checked";
                }?>>Sleeping</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="hobby[]" value="Cooking"<?php if(in_array("Cooking",$_hobby)){
                    echo "checked";
                }?>>Cooking</label>
        </div>
        <input type="submit" value="Update" class="btn btn-primary">
    </form>


</body>
</html>