<?php
include_once('../../../vendor/autoload.php');

use App\Bitm\SEIP139942\Hobby\Hobby;
use App\Bitm\SEIP139942\Utility\Utility;
use App\Bitm\SEIP139942\Message\Message;

session_start();
$hobby=new Hobby();
//$allHobbies=$hobby->index();

if(array_key_exists('itemPerPage',$_SESSION)) {
    if (array_key_exists('itemPerPage', $_GET)) {
        $_SESSION['itemPerPage'] = $_GET['itemPerPage'];
    }
}
else{
    $_SESSION['itemPerPage']=5;
}
$itemPerPage=$_SESSION['itemPerPage'];
$totalItem=$hobby->count();
$totalPage=ceil($totalItem/$itemPerPage);
//Utility::dd($itemPerPage);
$pagination="";
if(array_key_exists('pageNumber',$_GET)){
    $pageNumber=$_GET['pageNumber'];
}else{
    $pageNumber=1;
}
for($i=1;$i<=$totalPage;$i++){
    $class=($pageNumber==$i)?"active":"";
    $pagination.="<li class='$class'><a href='index.php?pageNumber=$i'>$i</a></li>";
}

$pageStartFrom=$itemPerPage*($pageNumber-1);
$allHobbies=$hobby->paginator($pageStartFrom,$itemPerPage);
$prev=$pageNumber-1;
$next=$pageNumber+1;
$previous="<li><a href='index.php?pageNumber=$prev'>Prev</a></li>";
$next="<li><a href='index.php?pageNumber=$next'>Next</a></li>";


?>

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>



<div class="container">
    <h2>List of Hobbies</h2>
    <a href="create.php" class="btn btn-info btn-lg" role="button">Enter Hobbies</a>
    <a href="trashed_view.php" class="btn btn-primary btn-lg" role="button">Trashed List</a><br><br>
    <a href="pdf.php" class="btn btn-info btn-lg" role="button">Download as PDF</a>
    <a href="excel.php" class="btn btn-success btn-lg" role="button">Download as Excel</a>
    <a href="email.php" class="btn btn-primary btn-lg" role="button">Email to Friend</a>
    <div id="message">
        <?php  if((array_key_exists('message',$_SESSION)&& (!empty($_SESSION['message'])))) {
            echo Message::message();
        }?>
    </div>
    <form role="form">
        <div class="form-group">
            <label for="sel1">Select items you want to show:</label>
            <select class="form-control" id="sel1" name="itemPerPage">
                <option <?php if($itemPerPage==5){?> selected="selected" <?php } ?> >5</option>
                <option <?php if($itemPerPage==10){?> selected="selected" <?php } ?>>10</option>
                <option <?php if($itemPerPage==15){?> selected="selected" <?php } ?>>15</option>
                <option <?php if($itemPerPage==20){?> selected="selected" <?php } ?>>20</option>
                <option <?php if($itemPerPage==25){?> selected="selected" <?php } ?>>25</option>
            </select>
            <button type="submit" class="btn btn-primary btn-sm">Go</button>

        </div>
    </form>
    <br><br>
    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th>#</th>
                <th>ID</th>
                <th>Hobbies</th>
                <th>Action</th>

            </tr>
            </thead>
            <tbody>
            <tr>
                <?php
                $sl=0;
                foreach($allHobbies as $hobby){
                $sl++;
                ?>
                <td><?php echo $sl+$pageStartFrom?></td>
                <td><?php echo $hobby['id']?></td>
                <td><?php echo $hobby['hobbies']?></td>
                <td><a href="view.php?id=<?php echo $hobby['id'] ?>" class="btn btn-primary" role="button">View</a>
                    <a href="edit.php?id=<?php echo $hobby['id']?>" class="btn btn-info" role="button">Edit</a>
                    <a href="trash.php?id=<?php echo $hobby['id']?>" class="btn btn-primary" role="button">Trash</a>
                    <a href="delete.php?id=<?php echo $hobby['id']?>" class="btn btn-danger" role="button">Delete</a>
                </td>


            </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
    <div class="text-center">
        <ul class="pagination">
            <?php if($pageNumber>1) echo $previous?>
            <?php echo $pagination?>
            <?php if($pageNumber!=$totalPage) echo $next?>
        </ul>
    </div>
</div>


<script>
    $('#message').show().delay(2000).fadeOut()
</script>

</body>
</html>