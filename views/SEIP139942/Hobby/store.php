<?php
include_once "../../../vendor/autoload.php";
use App\Bitm\SEIP139942\Hobby\Hobby;

$selected_hobby=$_POST['hobby'];
$comma_separated=implode(",",$selected_hobby);
$_POST['hobby']=$comma_separated;

$hobby=new Hobby();
$hobby->prepare($_POST);
$hobby->store();
