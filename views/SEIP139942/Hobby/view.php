<?php
include_once "../../../vendor/autoload.php";
use App\Bitm\SEIP139942\Hobby\Hobby;

$hobby=new hobby();
$hobby->prepare($_GET);
$singleItem=$hobby->view();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>View Hobby</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2><b>View Hobbies</b></h2>
    <ul class="list-group">
        <li class="list-group-item list-group-item-success"> <b>ID:</b> <?php echo $singleItem['id'] ?></li>
        <li class="list-group-item list-group-item-success"><b>Hobbies:</b> <?php echo $singleItem['hobbies']?></li>
    </ul>
</div>

</body>
</html>