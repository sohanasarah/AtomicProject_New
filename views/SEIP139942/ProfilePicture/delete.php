<?php
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP139942\ProfilePicture\ImageUploader;
use App\Bitm\SEIP139942\Utility\Utility;

$profile_picture= new ImageUploader();
$singleItem=$profile_picture->prepare($_GET);
$singleItem=$profile_picture->view();
unlink($_SERVER['DOCUMENT_ROOT'].'/AtomicProjectB22_Sarah_139942/Resources/Images/'.$singleItem['images']);
$singleItem=$profile_picture->prepare($_GET)->delete();