<?php
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP139942\ProfilePicture\ImageUploader;
use App\Bitm\SEIP139942\Utility\Utility;
$pro_pic= new ImageUploader();
$trashedInfo=$pro_pic->trashed();
//Utility::d($allBook);

?>

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Trash List</h2>
    <a href="index.php" class="btn btn-primary btn-lg" role="button">List of profile picture</a>
    <br><br>
    <form action="recover_multiple.php" method="post" id="multiple">
        <button type="submit" class="btn btn-info btn-lg"> Recover selected</button>
        <button type="button" class="btn btn-danger btn-lg" id="delete"> Delete selected</button>
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Profile Picture</th>
                    <th>Action</th>

                </tr>
                </thead>
                <tbody>
                <tr>
                    <?php
                    $sl=0;
                    foreach($trashedInfo as $info){
                    $sl++; ?>
                    <td><input type="checkbox" name=mark[] value="<?php echo $info['id'] ?>"></td>
                    <td><?php echo $sl?></td>
                    <td><?php echo $info['id']?></td>
                    <td><?php echo $info['name']?></td>
                    <td><img src="../../../Resources/Images/<?php echo $info['images'] ?>" alt="image" height="100px"
                             width="100px" class="img-responsive"></td>
                    <td><a href="recover.php?id=<?php echo $info['id'] ?>" class="btn btn-primary" role="button">Recover</a>
                        <a href="delete.php?id=<?php echo $info['id'] ?>"  class="btn btn-info" role="button">Delete</a>
                    </td>

                </tr>
                <?php }?>


                </tbody>
            </table>
        </div>

    </form>

</div>
<script>
    $('#delete').on('click',function(){
        document.forms[0].action="delete_multiple.php";
        $('#multiple').submit();
    });
</script>

</body>
</html>