<?php
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP139942\ProfilePicture\ImageUploader;
use App\Bitm\SEIP139942\Book\Utility;

$profile_picture= new ImageUploader();
$singleItem=$profile_picture->prepare($_GET)->view();
//Utility::d($singleItem);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>View Pictures</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2 align="center">View Profile Picture</h2>
    <ul class="list-group">
        <li class="list-group-item list-group-item-info"><b>ID:</b> <?php echo $singleItem['id']?></li>
        <li class="list-group-item list-group-item-info"><b>Name:</b> <?php echo $singleItem['name']?></li>
        <li class="list-group-item list-group-item-info"><b>Image: </b> <img src="../../../Resources/Images/<?php echo
            $singleItem['images']?>"</li>

    </ul>
</div>

</body>
</html>