<!DOCTYPE html>
<html lang="en">
<head>
    <title>Newsletter</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2><b>Enter your mail to subscribe</b></h2>
    <br>
    <form role="form" action="store.php" method="post">
        <div class="form-group">
            <label>Your Email Address: </label>
            <input type="text" name="email" class="form-control" id="mail" placeholder="Enter email">
        </div>
        <button type="submit" class="btn btn-info">Submit</button>
    </form>
</div>

</body>
</html>