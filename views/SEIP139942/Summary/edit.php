<?php
include_once "../../../vendor/autoload.php";
use App\Bitm\SEIP139942\Summary\Summary;
use App\Bitm\SEIP139942\Utility\Utility;

$org=new Summary();
$org->prepare($_GET);
$singleItem=$org->view();
//Utility::dd($singleItem);
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Summary</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2><b>Edit Summary</b></h2>
    <form role="form" action="update.php" method="post">
        <input type="hidden" name="id" id="comment" value="<?php echo $singleItem['id'] ?>">
        <label>Organization Name:</label>
        <input type="text" name="org" class="form-control" value="<?php echo $singleItem['organization']?>">
        <div class="form-group">
            <label for="comment">Summary:</label>
            <textarea class="form-control" rows="5" id="comment" name="summary" ?><?php echo $singleItem['summary'] ?></textarea>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>

</body>
</html>
