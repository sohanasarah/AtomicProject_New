<?php
include_once "../../../vendor/autoload.php";
use App\Bitm\SEIP139942\Summary\Summary;
use App\Bitm\SEIP139942\Utility\Utility;

$org=new Summary();
$org->prepare($_GET);
$singleItem=$org->view();
//Utility::dd($singleItem);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>View Summary</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2><b>View Summary</b></h2>
    <br> <br>
    <ul class="list-group">
        <li class="list-group-item list-group-item-success"><b>ID: </b><?php echo $singleItem['id'] ?></li>
        <li class="list-group-item list-group-item-success"><b>Organization: </b><?php echo $singleItem['organization'] ?></li>
        <li class="list-group-item list-group-item-success "><b>Summary: </b><br><?php echo $singleItem['summary'] ?></li>
    </ul>
</div>

</body>
</html>